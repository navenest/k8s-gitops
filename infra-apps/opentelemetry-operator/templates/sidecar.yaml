apiVersion: opentelemetry.io/v1alpha1
kind: OpenTelemetryCollector
metadata:
  name: default-sidecar
spec:
  mode: sidecar
  resources:
    limits:
      memory: 125M
    requests:
      cpu: 50m
      memory: 125M
  config: |
    receivers:
      jaeger:
        protocols:
          thrift_http:
      otlp:
        protocols:
          grpc:
          http:
      
    processors:
      memory_limiter:
        check_interval: 1s
        limit_percentage: 75
        spike_limit_percentage: 15
      batch:
        send_batch_size: 10000
        timeout: 10s
      tail_sampling:
        decision_wait: 60s
        num_traces: 1000
        expected_new_traces_per_sec: 0
        policies:
          [
            {
              name: latency,
              type: latency,
              latency:
              {
                threshold_ms: 2000
              }
            },
            {
              name: errors,
              type: status_code,
              status_code: 
              {
                status_codes:
                [
                  ERROR
                ]
              }
            },
            {
              name: happy-path-filter,
              type: and,
              and:
              {
                and_sub_policy:
                [
                  {
                    name: ignore-probes,
                    type: string_attribute,
                    string_attribute:
                    {
                      key: http.user_agent,
                      values:
                      [
                        kube-probe/.*,
                        Prometheus/.*,
                      ],
                      enabled_regex_matching: true,
                      invert_match: true
                    }
                  },
                  {
                    name: probability,
                    type: probabilistic,
                    probabilistic: 
                    {
                      sampling_percentage: 10
                    }
                  }
                ]
              }
            }
          ]

    exporters:
      logging:
      otlp:
        endpoint: default-collector.opentelemetry-operator-system:4317
        tls:
          insecure: true
    
    extensions:
      health_check: {}
      memory_ballast: {}

    service:
      telemetry:
        metrics:
          address: 0.0.0.0:8888
      extensions:
        - health_check
        - memory_ballast
      pipelines:
        metrics:
          receivers:
            - otlp
          processors: 
            - memory_limiter
            - batch
          exporters: 
            - logging
            - otlp
        traces:
          receivers:
            - otlp
            - jaeger
          processors: 
            - memory_limiter
            - batch
            - tail_sampling
          exporters: 
            - logging
            - otlp