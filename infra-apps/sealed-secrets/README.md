# Sealed Secrets

```bash
helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
helm upgrade --install --namespace kube-system sealed-secrets sealed-secrets/sealed-secrets -f values.yaml
kubeseal --controller-namespace=kube-system --controller-name=sealed-secrets -f ./.secrets/pdns-api-secret.yaml -w sealedsecret-pdns-api-secret.yaml
```