# ARGOCD

```bash
kubectl create ns argocd
kubectl apply -f argo/sealedsecret.yaml
helm repo add argo https://argoproj.github.io/argo-helm
helm upgrade --install argo-cd argo/argo-cd -n argocd -f values.yaml
kubectl apply -f argo/project.yaml
kubectl apply -f argo/application.yaml
```