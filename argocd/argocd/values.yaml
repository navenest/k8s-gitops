argo-cd:
  controller:
    replicas: 2
    enableStatefulSet: true

    env:
      - name: "ARGOCD_CONTROLLER_REPLICAS"
        value: "2"
    
    metrics:
      enabled: true
      applicationLabels:
        enabled: true
      serviceMonitor:
        enabled: true

  dex:
    livenessProbe:
      enabled: true
    readinessProbe:
      enabled: true
    metrics:
      enabled: true
      applicationLabels:
        enabled: true
      serviceMonitor:
        enabled: true

  redis:
    metrics:
      enabled: true
      applicationLabels:
        enabled: true
      serviceMonitor:
        enabled: true

  server:
    replicas: 2
    certificate:
      enabled: true
      domain: argocd.pineapplenest.com
      issuer:
        kind: ClusterIssuer
        name: le-cf-issuer
      additionalHosts: []
      secretName: argocd-server-tls

    metrics:
      enabled: true
      applicationLabels:
        enabled: true
      serviceMonitor:
        enabled: true

    ingress:
      enabled: true
      annotations: 
        nginx.ingress.kubernetes.io/backend-protocol:  "HTTPS"
      labels: {}
      ingressClassName: "nginx"

      ## Argo Ingress.
      ## Hostnames must be provided if Ingress is enabled.
      ## Secrets must be manually created in the namespace
      ##
      hosts:
        - argocd.pineapplenest.com
      paths:
        - /
      pathType: Prefix
      extraPaths:
        []
        # - path: /*
        #   backend:
        #     serviceName: ssl-redirect
        #     servicePort: use-annotation
        ## for Kubernetes >=1.19 (when "networking.k8s.io/v1" is used)
        # - path: /*
        #   pathType: Prefix
        #   backend:
        #     service:
        #       name: ssl-redirect
        #       port:
        #         name: use-annotation
      tls:
        - secretName: argocd-server-tls
          hosts:
            - argocd.pineapplenest.com
      https: true

    config:
      # Argo CD's externally facing base URL (optional). Required when configuring SSO
      url: https://argocd.pineapplenest.com
      # Argo CD instance label key
      application.instanceLabelKey: argocd.argoproj.io/instance
      resource.customizations: |
        bitnami.com/SealedSecret:
          health.lua: |
            hs = {}
            hs.status = "Healthy"
            hs.message = "Controller doesn't report resource status"
            return hs
      repositories: |
        - url: https://gitlab.com/navenest/k8s-gitops.git
          passwordSecret:
            name: gitlab-k8s-gitops
            key: password
          usernameSecret:
            name: gitlab-k8s-gitops
            key: username
        - type: helm
          url: https://argoproj.github.io/argo-helm
          name: argo
        - type: helm
          url: https://charts.bitnami.com/bitnami
          name: bitnami
      dex.config: |
        connectors:
          - type: oidc
            name: KeyCloak
            id: keycloak
            config:
              issuer: https://keycloak.pineapplenest.com/realms/master
              clientID: argocd
              clientSecret: $keycloak-client:clientSecret
              redirectURI: https://argocd.pineapplenest.com/api/dex/callback
              insecureEnableGroups: true
              scopes:
              - profile
              - email
              - groups
              - openid

    rbacConfig:
      policy.default: role:readonly
      policy.csv: |
        g, ArgoCDAdmin, role:admin


  repoServer:
    replicas: 3
    env:
      - name: ARGOCD_EXEC_TIMEOUT
        value: 240s
    containerSecurityContext:
      runAsNonRoot: true
      readOnlyRootFilesystem: true
      allowPrivilegeEscalation: false
      seccompProfile:
        type: Unconfined
      capabilities:
        drop:
        - ALL
    metrics:
      enabled: true
      applicationLabels:
        enabled: true
      serviceMonitor:
        enabled: true
    logLevel: debug
    resources:
      limits:
        cpu: null
        memory: 3Gi
      requests:
        cpu: 1
        memory: 3Gi
  applicationSet:
    metrics:
      enabled: true
      serviceMonitor:
        enabled: true
    containerSecurityContext:
      runAsNonRoot: true
      readOnlyRootFilesystem: true
      allowPrivilegeEscalation: false
      seccompProfile:
        type: Unconfined
      capabilities:
        drop:
        - ALL
    readinessProbe:
      enabled: true
    livenessProbe:
      enabled: true
