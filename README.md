# k8s-gitops

## Sealed Secrets

```
kubeseal --controller-name sealed-secrets --secret-file .\.secrets\renovate-bot-secret.yaml -w infra-apps-secrets/sealedsecret-renovate-bot-secret.yaml

kubeseal --controller-name sealed-secrets --secret-file .\.secrets\ghost-secret.yaml -w apps-secrets/sealedsecret-ghost-secret.yaml

kubeseal --controller-name sealed-secrets --secret-file .\.secrets\keycloak.yaml -w apps-secrets/sealedsecret-keycloak.yaml

kubeseal --controller-name sealed-secrets --secret-file .\.secrets\keycloak-postgres.yaml -w apps-secrets/sealedsecret-keycloak-postgres.yaml

kubeseal --controller-name sealed-secrets --secret-file .\.secrets\vaultwarden-secret.yaml -w apps-secrets/sealedsecret-vaultwarden-secret.yaml

kubeseal --controller-name sealed-secrets --secret-file .\.secrets\prometheus-notifications.yaml -w infra-apps-secrets/sealedsecret-prometheus-notifications.yaml

kubeseal --controller-name sealed-secrets --secret-file .\.secrets\cloudflare-api-key-secret.yaml -w infra-apps-secrets/sealedsecret-cloudflare-api-key-secret.yaml

kubeseal --controller-name sealed-secrets --secret-file .\.secrets\argocd-secret.yaml -w argocd/setup/sealedsecret.yaml

kubeseal --controller-name sealed-secrets --secret-file .\.secrets\argocd-notifications.yaml -w argocd/setup/sealedsecret-argocd-notifications.yaml
```